##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
DEFINE re 	util.REGEX
DEFINE m 	util.MATCH_RESULTS
DEFINE rs 	STRING
DEFINE r 	BOOL

LET re =~/(\w+)\s(\w+)/

LET r = util.REGEX.match("Test Libraries",re)
DISPLAY "Match result of two words with space in 'Test Libraries': ", r

LET m = util.REGEX.search("Test Libraries",re)
IF (m) THEN
  DISPLAY "Match result size:", m.size()
  DISPLAY "Match result position:", m.position(0)
  DISPLAY "Match result length:",m.length(0)
  DISPLAY "Match result string:", m.str(0)
END IF

LET re = ~/a|e|i|o|u/
LET rs = util.REGEX.replace("Test string",re, "[$&]"); 
DISPLAY rs
END MAIN