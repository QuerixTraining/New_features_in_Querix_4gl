##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE arr util.JSONArray
    DEFINE da DYNAMIC ARRAY OF STRING
    LET arr = util.JSONArray.Create()
    
    CALL arr.put(1, "007")
    CALL arr.put(2, "Bond")
    CALL arr.put(3, "James")
    
    DISPLAY "JSON array: ", arr.toString()
    DISPLAY "JSON array length: ", arr.getLength()
    DISPLAY "JSON array first element type: ", arr.getType(1)
    DISPLAY "JSON array second element: ", arr.get(2)
    CALL arr.remove(3)
    DISPLAY "JSON array after removing 3rd item: ", arr.toString()
    
    CALL  arr.toFGL(da)
    DISPLAY "DYNAMIC ARRAY items: ", da[1], ", ", da[2]
    
    
    
END MAIN