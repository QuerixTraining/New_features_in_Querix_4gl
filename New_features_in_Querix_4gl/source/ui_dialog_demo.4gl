##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE f1 STRING
	DEFINE d ui.Dialog
	DEFINE counter INT
	DEFINE tab1 ARRAY[45] OF RECORD
							f1 INT,
							f2 INT
						END RECORD
	
	MENU

		ON ACTION "simple_input" 
			OPEN WINDOW w1 WITH FORM "ui_dialog_demo" ATTRIBUTE (BORDER)
			
			LET counter = 0       
			
			INPUT BY NAME f1 
				
				ON ACTION "add_1"
					LET counter = counter + 1			
					DISPLAY counter
					
				ON ACTION "Enable button"			
					LET d = ui.Dialog.GetCurrent()
			        CALL d.setActionActive( "add_1", TRUE )
							
				ON ACTION "Disable button"	
				    LET d = ui.Dialog.GetCurrent()
				    CALL d.setActionActive( "add_1", FALSE )
				
				ON ACTION "Hide action"		    
				    CALL d.SetActionHidden( "add_1", TRUE )
					
				ON ACTION "exit"
					EXIT INPUT
				
			END INPUT
			CLOSE WINDOW w1
		
		ON ACTION "input_array"
			OPEN WINDOW w2 WITH FORM "ui_dialog_demo_array" ATTRIBUTE (BORDER)
			
			FOR counter = 1 TO 45
				LET tab1[counter].f1 = counter  
				LET tab1[counter].f2 = counter
			END FOR   
			CALL SET_COUNT(45)
			INPUT ARRAY tab1 from tab1.* ATTRIBUTES (WITHOUT DEFAULTS)
				ON ACTION "Current_row"
					LET d = ui.Dialog.GetCurrent()
				    DISPLAY d.getCurrentRow("tab1")	
				ON ACTION "SetRow"
					LET d = ui.Dialog.GetCurrent()
				    CALL d.setCurrentRow("tab1", 10)
				ON ACTION "GetItem"
					LET d = ui.Dialog.GetCurrent()
				    DISPLAY d.getCurrentItem()
				ON ACTION "GetArrayLength"
					LET d = ui.Dialog.GetCurrent()
				    DISPLAY d.getArrayLength("tab1")	    	
			END INPUT	    		
			CLOSE WINDOW w1
		
		ON ACTION "exit_app"
			EXIT MENU		
	END MENU		
END MAIN	

