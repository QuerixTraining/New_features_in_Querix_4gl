##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	
	DEFINE customer RECORD
               id INTEGER,
               lastname VARCHAR(30),
               order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD

    LET customer.id = 007
    LET customer.lastname = "Bond"
    LET customer.order_ids[1] = 100
    LET customer.order_ids[2] = 101
    LET customer.order_ids[3] = 102
    
    
    DISPLAY customer
    
END MAIN