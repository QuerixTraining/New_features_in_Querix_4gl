##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN

	DEFINE darr DYNAMIC ARRAY OF INT # No need to specify size of dynamic array, it can be up to 2147483647
	DEFINE sarr ARRAY [10] OF INT    # You have to specify size of an array and it's maximum is 32767
    DEFINE i INT
	DEFINE el CHAR(5)
	DEFINE value INT
	DEFINE def STRING
	
	FOR i = 1 TO 10
		LET darr[i] = i
		LET sarr[i] = i
	END FOR	
	
	OPEN WINDOW w1 WITH FORM 'dynamic_array_input_array' ATTRIBUTES (BORDER)
	
	MENU
		ON ACTION 'act_disp_dyn'
		    LET def = "No need to specify the total number of records"
			DISPLAY def TO lb1
			DISPLAY ARRAY darr TO tab.*
			END DISPLAY 
		ON ACTION 'act_inp_dyn'
		    LET def = "When used in the INPUT ARRAY statement, \nthe array will be automatically resized when the cursor moves to a \nnon occupied row of the screen array as well as when the row is deleted."
		    DISPLAY def TO lb1
		    INPUT ARRAY darr WITHOUT DEFAULTS FROM tab.*	
			END INPUT
		ON ACTION 'act_disp_stat'
		    LET def = "You have to specify the quantity of the program records" 
			DISPLAY def TO lb1
		    CALL SET_COUNT(10) 
			DISPLAY ARRAY sarr TO tab.*
			END DISPLAY 
		ON ACTION 'act_inp_stat'
			LET def = "You can't append or insert new row until you'll delete one of them"
		    DISPLAY def TO lb1
		    CALL SET_COUNT(10)
			INPUT ARRAY sarr WITHOUT DEFAULTS FROM tab.*	
			END INPUT
		ON ACTION 'act_add' # You can use build-in methods to control the dynamic array
			LET i = darr.getSize()
			CALL darr.append(i+1)	
			DISPLAY "Dynamic array: \n", darr
		ON ACTION 'act_del' # You can use build-in methods to control the dynamic array
			PROMPT "Input number of row from what to start deleting: " FOR el
			PROMPT "Input amount of rows to delete: " FOR value
			CALL darr.delete(el, value)
			DISPLAY "Dynamic array: \n", darr	
		ON ACTION 'act_insrt' # You can use build-in methods to control the dynamic array
			PROMPT "Input number of row you want to insert: " FOR el
			PROMPT "Input value of array element: " FOR value
			CALL darr.insert(el,value)
			DISPLAY "Dynamic array: \n", darr
		ON ACTION 'act_consl'
			DISPLAY "Dynamic array: \n", darr			
		ON ACTION 'exit'
			EXIT MENU
	END MENU
	
END MAIN