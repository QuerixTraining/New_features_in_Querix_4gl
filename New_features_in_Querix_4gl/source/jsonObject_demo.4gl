##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE obj util.JSONObject
	DEFINE customer RECORD
               id INT,
         lastname VARCHAR(30)
           END RECORD
    
    LET obj = util.JSONObject.CREATE()
    
    CALL obj.put("identifier", 7)
    CALL obj.put("surname", "Bond")
    CALL obj.put("orders", [123, 456, 789])
    
    DISPLAY "JSON object: ", obj.toString()
    DISPLAY "JSON object length: ", obj.getLength()
    DISPLAY "JSON object has element identifier: ", obj.has("identifier")
    CALL obj.remove("orders")
    DISPLAY "JSON object after removing orders: ", obj.toString()
    DISPLAY "JSON Object property name: ", obj.name(1)
    CALL obj.toFGL(customer)
    DISPLAY "RECORD items: ", customer.id, ", ", customer.lastname
    
    
    
END MAIN