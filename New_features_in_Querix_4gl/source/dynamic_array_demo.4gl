##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

	DEFINE arr ARRAY [5] OF INT
	DEFINE darr DYNAMIC ARRAY OF INT
	DEFINE dddarr DYNAMIC  ARRAY WITH 2 DIMENSIONS OF INT
    DEFINE i, j, arrLength INT
	
	LET arrLength = 5
	FOR i = 1 TO arrLength
		LET arr[i] = i
	END FOR
	
	DISPLAY "Static array:"	
	FOR i = 1 TO arrLength
		DISPLAY arr[i]
	END FOR

	
	FOR i = 1 TO arrLength
		LET darr[i] = i
	END FOR	
	DISPLAY "\nDynamic array: \n", darr
	
	FOR i = 1 TO 5
		FOR j = 1 TO 5
			LET dddarr[i,j] = i*j
		END FOR
	END FOR	
	DISPLAY "\nDouble-dimensioned dynamic array: \n", dddarr
	DISPLAY "or:"
	FOR i = 1 TO arrLength
		DISPLAY dddarr[i]
	END FOR
	
END MAIN