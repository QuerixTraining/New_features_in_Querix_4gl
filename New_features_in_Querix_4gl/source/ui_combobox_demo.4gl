##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


MAIN
	DEFINE f1 ui.ComboBox
	DEFINE s1 STRING
	DEFINE size ui.Size

    OPEN WINDOW w1 WITH FORM  "ui_combobox_demo" ATTRIBUTE(BORDER)

	LET f1 = ui.ComboBox.ForName("f1")
	
	CALL f1.SetPreferredSize(["100px","22px"])
	CALL f1.SetLocation(["10px","10px"])

	INPUT s1 FROM f1
	DISPLAY s1
	
	CALL f1.SetComboBoxValues(["first_value","second_value"])
	INPUT s1 FROM f1
	DISPLAY s1
	
	CALL f1.AddItem("3","third_value")
	INPUT s1 FROM f1
	DISPLAY s1

	CALL f1.RemoveItem("1")
	INPUT s1 FROM f1
	DISPLAY s1
	
	DISPLAY "Text of 3: ", f1.GetTextOf("3")
	CALL fgl_getkey()
END MAIN