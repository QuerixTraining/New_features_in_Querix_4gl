##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE f1 STRING

MAIN

	OPTIONS FORM LINE FIRST
MENU	
	
	ON ACTION "grid"
		OPEN WINDOW w1 WITH FORM "messagesjs_demo_grid" ATTRIBUTE (BORDER)
	    CALL ui.interface.frontcall("html5","scriptImport",["qx://application/scripts/messages.js"],[])
        CALL ui.interface.frontcall("html5","styleImport",["qx://application/messages.css"],[])  
		MENU
			ON ACTION "PopUp Error"
		    	ERROR "this is ERROR"
		    	
		    ON ACTION "PopUp Message"
		    	MESSAGE "this is MESSAGE"
		    	
		    ON ACTION  "Coords_1_9"
		    	DISPLAY "DISPLAY AT 1,9" AT 1,9 
	
		    ON ACTION "Coords_5_5"
		    	DISPLAY "DISPLAY AT 5,5" AT 5,5
		    	
		    ON ACTION "Coords_8_2"
		    	DISPLAY "DISPLAY AT 8,2" AT 8,2
			
			ON ACTION exit
				EXIT MENU
		END MENU
		INPUT BY NAME f1
		    ON ACTION "To focused field"
		    	DISPLAY "DISPLAY AT 6,6" AT 6,6
		END INPUT
        CLOSE WINDOW w1
        
	ON ACTION "coord"
		OPEN WINDOW w2 WITH FORM "messagesjs_demo_coord" ATTRIBUTE (BORDER)
	

		MENU
			ON ACTION "PopUp Error"
		    	ERROR "this is ERROR"
		    	
		    ON ACTION "PopUp Message"
		    	MESSAGE "this is MESSAGE"
		    	
		    ON ACTION  "Coords_1_9"
		    	DISPLAY "DISPLAY AT 1,9" AT 1,9 
	
		    ON ACTION "Coords_5_5"
		    	DISPLAY "DISPLAY AT 5,5" AT 5,5
		    	
		    ON ACTION "Coords_10_1"
		    	DISPLAY "DISPLAY AT 10,1,10" AT 10,1
			ON ACTION exit
				EXIT MENU
		END MENU
		INPUT BY NAME f1
		    ON ACTION "To focused field"
		    	DISPLAY "DISPLAY AT 6,6" AT 6,6
		END INPUT
		CLOSE WINDOW w2
	ON ACTION "exit_main"
		EXIT MENU
END MENU		
END MAIN