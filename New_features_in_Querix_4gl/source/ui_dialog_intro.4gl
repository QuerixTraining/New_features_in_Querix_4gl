##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE f1 STRING
	DEFINE counter INT
	
	OPEN WINDOW w1 WITH FORM "ui_dialog_intro" ATTRIBUTE (BORDER)
	
	LET counter = 0       
	
	INPUT BY NAME f1 
	    
		ON ACTION "add_1"
			LET counter = counter + 1			
			DISPLAY counter
			
		ON ACTION "Enable button"			
	        CALL DIALOG.setActionActive( "add_1", TRUE )
					
		ON ACTION "Disable button"	
		    CALL DIALOG.setActionActive( "add_1", FALSE )
			
		ON ACTION "exit"
			EXIT INPUT
		
	END INPUT
			
END MAIN	

