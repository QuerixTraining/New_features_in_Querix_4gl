##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DATABASE cms

 DEFINE err_code INTEGER --variable keeps execution code of method
 DEFINE res_rec RECORD 
					field1 INTEGER,
					field2 CHAR(20)	
				END RECORD
MAIN
 DEFINE cur CURSOR
 CALL populate_temp_table() -- create table for our example

 CALL cur.Declare("SELECT * FROM tmp_cur WHERE field2 MATCHES ?",1) RETURNING err_code --declare cursor with scroll
 CALL cur.SetParameters("odd*") -- set parameter to declared query
 CALL cur.SetResults(res_rec.*) RETURNING err_code --define variable for result output
 CALL cur.Open() RETURNING err_code
 
 CALL expect_cursor(cur) --we may pass cursor to function and continue working with it inside function
 
 CALL cur.FetchFirst() RETURNING err_code --get first row
 DISPLAY "First row:",res_rec
 CALL cur.FetchLast() RETURNING err_code --get last row
 DISPLAY "Last row:",res_rec
 
 CALL cur.Close() RETURNING err_code
 CALL handler(cur.Free()) --you may handle returned code by passing RETURNING value to user function in this way
END MAIN
#########################
FUNCTION expect_cursor(cur_local)
DEFINE cur_local CURSOR
	WHILE TRUE
		IF cur_local.FetchNext() THEN EXIT WHILE END IF --get one by one row while we have returned data by declared query
		DISPLAY res_rec
	END WHILE
END FUNCTION
#########################
FUNCTION handler(err_code)
 DEFINE err_code INTEGER
 IF err_code <> 0 THEN
 	DISPLAY "Error code:",err_code
 END IF
END FUNCTION
##########################
FUNCTION populate_temp_table()
 DEFINE i INTEGER
 DEFINE odd_even CHAR(20)
	CREATE TEMP TABLE tmp_cur(
		field1 INTEGER,
		field2 CHAR(20)
		)
 FOR i=1 TO 100
 	IF i mod 2 = 0 THEN
 		LET odd_even = "even", trim(i)
 	 	INSERT INTO tmp_cur VALUES(i,odd_even)
	ELSE
		LET odd_even = "odd", trim(i)
		INSERT INTO tmp_cur VALUES(i,odd_even)
	END IF
 END FOR
END FUNCTION