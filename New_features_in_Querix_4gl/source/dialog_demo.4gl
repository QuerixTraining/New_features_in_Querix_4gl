##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
	DEFINE tarr1 DYNAMIC ARRAY OF RECORD f1,f2 CHAR(10) END RECORD
	DEFINE tarr2 DYNAMIC ARRAY OF RECORD f3,f4 CHAR(10) END RECORD
	 
	LET tarr1[1].f1="1"
	LET tarr1[1].f2="One"
	LET tarr2[1].f3="1"
	LET tarr2[1].f4="Ein"
	LET tarr1[2].f1="2"
	LET tarr1[2].f2="Two"
	LET tarr2[2].f3="2"
	LET tarr2[2].f4="Zwei"
	 
	MENU
	 	ON ACTION "no_dialog"
	 		OPEN WINDOW w1 WITH FORM "dialog_demo" ATTRIBUTE(BORDER)
	 		DISPLAY ARRAY tarr1 TO tab1.* END DISPLAY
			INPUT ARRAY tarr2 FROM tab2.* END INPUT
	 		CLOSE WINDOW w1
	
	 	ON ACTION "dialog"
		 	OPEN WINDOW w1 WITH FORM "dialog_demo" ATTRIBUTE(BORDER)
			 
			DIALOG
				DISPLAY ARRAY tarr1 TO tab1.* END DISPLAY
				INPUT ARRAY tarr2 FROM tab2.* END INPUT
				
				ON ACTION "hi"
					DISPLAY "Hi, my dear friend!"
					
				ON ACTION Exit
			    	EXIT DIALOG
			
			END DIALOG
			CLOSE WINDOW w1
	
		ON ACTION "exit_app"
			EXIT MENU
	END MENU		
END MAIN

