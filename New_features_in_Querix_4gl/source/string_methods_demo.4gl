##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE s STRING
MAIN

	LET s = "123456789"
	
	DISPLAY "Basic string: |"||s||"|\n"
	DISPLAY "Appended string: |"||s.append("0000")||"|"
	DISPLAY "String equals to '12345': ", s.equals('12345')  
    DISPLAY "String equals to '123456789': ", s.equals('123456789')
	DISPLAY "Char at 5th position: ", s.getCharAt(5)
	--DISPLAY "Index of number 5: ", s.getIndexOf('5')
	DISPLAY "Length of string: ", s.getLength()
	
	LET s = "aBcDeF"
	DISPLAY "\nNew string: ", s, "\n"
	DISPLAY "To lower case: ", s.toLowerCase()
	DISPLAY "To upper case: ", s.toUpperCase()
	
	LET s = "    a b c d e f    "
	DISPLAY "\nNew string: |"||s||"|\n"
	DISPLAY "Trimmed string: |"||s.trim()||"|"  
	DISPLAY "Trimmed left string: |"||s.trimLeft()||"|"
	DISPLAY "Trimmed right string: |"||s.trimRight()||"|"

END MAIN