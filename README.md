This demo project is intended to show and display new functionality of Querix 4GL:
STRING methods implemented for manipulating character strings;
DYNAMIC ARRAY methods implemented for handling arrays without their fixed size;
JSON as a lightweight format for data interchange;
DIALOG statement used for combining different user interaction statements and executing them simultaneously;
ui methods provided by Lycia, 
regular expressions used for searching for and finding sequences of characters in longer character strings, and
message.js provided for manipulating labels.

Details about these Lycia features can be found in Lycia online documentation:
STRING - http://querix.com/go/docs_online/#lycia_reference/library/data_types/string.htm
DYNAMIC ARRAY - http://querix.com/go/docs_online/#lycia_reference/library/data_types/dynamic_array/dynamic_array.htm
JSON - http://querix.com/go/docs_online/#auxiliary_features/json/json_intro.htm
DIALOG - http://querix.com/go/docs_online/#lycia_reference/library/statements/dialog/dialog.htm
ui methods - http://querix.com/go/docs_online/#lycia_reference/library/classes_and_methods/ui/ui_methods.htm
regular expressions - http://querix.com/go/docs_online/#lycia_reference/library/regular_expressions/regular_expressions.htm

To be able to try and use this project, you have to perform the following steps.
1. Register at Querix web-site: http://querix.com/register/
2. Download the latest Lycia III version: http://querix.com/lycia/downloads/
3. Install Lycia to your computer: http://querix.com/go/docs_online/#installation/win_install.htm 
4. Activate the trial license (1 license per 1 developer): http://querix.com/go/docs_online/#licensing/trial.htm (Please contact us if you need more compilation seats. We'd also appreciate it if you let us know the number of developers who will be engaged in this project.)
5. Import this project from the GIT repository via LyciaStudio: http://querix.com/go/docs_online/#developers_guide/working_with_repositories/git/git_perspective_and_views/git_repos_view/clone_repo.htm

Use the link below to download the source files. Here is the link to be cloned: https://gitlab.com/QuerixTraining/New_features_in_Querix_4gl.git

You can read about working with GIT repositories in Lycia online documentation: http://querix.com/go/docs_online/#developers_guide/working_with_repositories/repos.htm

After importing this demo project to your workspace, you have to build and run it from LyciaStudio:
http://querix.com/go/docs_online/lyciastudio/build/building_projects/building_and_compilation.htm
http://querix.com/go/docs_online/lyciastudio/run/running_applications/running_applications.htm

Please clear your browsing data before launching the application.

######################################

This group contains demo applications for Querix products.
This software is free but can be used and modified only with Lycia.
Feel free to give your suggestions about enriching this site. 
You can send your ideas and 4gl code to support@querix.com

